//6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).
function averagefAllSalariesBasedOnCountry(data) {
  if (Array.isArray(data)) {
    // salaries of count by country
    let countOfCountry = {};
    // average of salaries by country
    let averageOfAllSalariesByCountry = {};
    // summation by country wise
    let allSalariesByCountry = {};
    for (let index = 0; index < data.length; index++) {
        // get the country name 
      const country = data[index].location;
      // get the salary in the form of integer 
      const salary = Number(data[index].salary.replace("$", ""));
      // retrive the salaries by country wise
      // ooccurance of the country 
      if (!allSalariesByCountry[country]) {
        allSalariesByCountry[country] = 0;
        countOfCountry[country] = 0;
      }
      allSalariesByCountry[country] += salary;
      countOfCountry[country]++;
    }
    // average of salaries group by countries 
    for (const country in allSalariesByCountry) {
      averageOfAllSalariesByCountry[country] =
        allSalariesByCountry[country] / countOfCountry[country];
    }
    return averageOfAllSalariesByCountry;
  } else {
    return "Data Insufficient ";
  }
}
// export the code
module.exports = {averagefAllSalariesBasedOnCountry}
