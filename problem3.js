// Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something.
let details = module.require("./data");
let data = details.details;
function eachSalaryAmountIsFactorOf10000(data) {
  if (Array.isArray(data)) {
    // store the salaries
    let salaries_In_Numbers = [];
    for (let index = 0; index < data.length; index++) {
      //remove the Dollar in the String
      const salaryInString = data[index].salary.replace("$", "");
      // Convert the String into Number
      const salaryInNumber = Number(salaryInString);
      // salary amount is a factor of 10000
      salaries_In_Numbers.push(Math.round(salaryInNumber * 10000));
      //add the new key as a "corrected_salary "
      data[index].corrected_salary = Math.round(salaryInNumber * 10000);
    }
    return salaries_In_Numbers;
  } else {
    return "Data Unsufficeint";
  }
}
// exports the code
module.exports = { eachSalaryAmountIsFactorOf10000 };
