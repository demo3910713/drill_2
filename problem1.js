//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
function allWebDevelopers(data) {
  // count the webDevelopers
  let count = 0;
  if (Array.isArray(data)) {
    //store the Details
    let info = [];
    for (let index = 0; index < data.length; index++) {
      const job = data[index].job;
      // filter the web Developer in the Data set
      if (job.includes("Web Developer")) {
        info[data[index].first_name + " " + data[index].last_name] =
          data[index].job;
        count++;
      }
    }
    return { info, count };
  } else {
    return " Data Insufficient";
  }
}
// exports the code
module.exports = { allWebDevelopers };
