//4. Find the sum of all salaries.
let details = module.require("./data");
let data = details.details;
function sumOfAllSalaries(data) {
  if (Array.isArray(data)) {
    // store the summation of the all salaries
    let allSalaries = 0;
    for (let index = 0; index < data.length; index++) {
      const salaryInString = data[index].salary.replace("$", "");
      // convertion String to  Number
      const salaryInNumber = Number(salaryInString);
      // adding all salaries
      allSalaries = salaryInNumber + allSalaries;
    }
    return allSalaries;
  } else {
    return "Data Unsufficeint";
  }
}
// export the code
module.exports = { sumOfAllSalaries };
