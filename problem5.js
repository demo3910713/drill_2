//5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).
let details = module.require("./data");
let data = details.details;
function sumOfAllSalariesBasedOnCountry(data) {
  if (Array.isArray(data)) {
    // store the sum of salaries by country wise
    let allSalariesByCountry = {};
    for (let index = 0; index < data.length; index++) {
      const country = data[index].location;
      //convert string to Number
      const salary = Number(data[index].salary.replace("$", ""));
      // insert into the allSalariesByCountry
      if (!allSalariesByCountry[country]) {
        allSalariesByCountry[country] = 0;
      }
      allSalariesByCountry[country] += salary;
    }
    return allSalariesByCountry;
  } else {
    return "Data Insufficient ";
  }
}
// export the code
module.exports = { sumOfAllSalariesBasedOnCountry}
