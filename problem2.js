
// Convert all the salary values into proper numbers instead of strings.
function allSalariesStringIntoNumber(data) {
    if(Array.isArray(data)) {
      // store the salaries
        let salaries_In_Numbers =[]
      for (let index = 0; index < data.length; index++) {
        // convert the string to numbers 
        const salary = Number(data[index].salary.replace('$',''));
        // push the salaries into the salaries_In_Numbers 
        salaries_In_Numbers.push(salary)
      }
      return salaries_In_Numbers
    }
    else
    {
        return("Data Unsufficeint")
    }
    
}
// Exports the Data
module.exports = { allSalariesStringIntoNumber}
